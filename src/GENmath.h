#pragma once
#ifndef GENMATH_H
#define GENMATH_H

//#include <xmmintrin.h> // old systems
#include <pmmintrin.h>

#include <cassert>
#include <cmath>
#include <cstdint>


// LEFTHANDED! Row-based matrix mult

class GENmath
{
public:
	template <typename T, char iterations = 2> 
	static inline T inv_sqrt(T x);

private:

};

#endif // GENMATH_H