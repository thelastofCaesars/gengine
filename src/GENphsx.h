#pragma once
#ifndef GENPHSX_H
#define GENPHSX_H

#include <cassert>
#include <cmath>

// search for NIST:Constants

class GENphx
{
	struct physicConstant
	{
		double c;
		/*
		enum physicType
		{
			length,
			mass,
			time,
			electricCurrent,
			thermodynamicTemperature,
			amountOfSubstance,
			luminousIntensity
		}
		*/
	};
	

	typedef double length;						// meter | m
	typedef double mass;						// kilogram | kg
	typedef double time;						// second | s
	typedef double electricCurrent;				// ampere | A
	typedef double thermodynamicTemperature;	// kelvin | K
	typedef double amountOfSubstance;			// mole | mol
	typedef double luminousIntensity;			// candela | cd

	const int		SpeedOfLightInVacuum = 299792458;	// m/s speed of light
	const double	GravitionalConstant = 6.6743015 * std::pow(10,-11);	// m^3/kg*s^2
	const float		StandardG = 9.80665;				// m/s^2
	const float		Inch = 0.0254;						// m
	const double	ElectronoVolt = 1.602176634 * std::pow(10, -19);	// J
	const float		BaseOfNaturalLog = 2.718281828;		// scrapped
	const float		GasConstant = 8.3144598;			// modular, ideal
};

#endif GENPHSX_H