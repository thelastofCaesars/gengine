#pragma once
#ifndef GGAMESTATE_H
#define GGAMESTATE_H

#include <iostream>
#include <cassert>

#include "GObject.h"
#include "GSystem.h"

class GGameState : GObject
{
public:
	GGameState();
	~GGameState();

	void UpdateSystems();

private:
	// GSystem _system; // base example
};

#endif // GGAMESTATE_H