#pragma once
#ifndef GMESSAGEBUS_H
#define GMESSAGEBUS_H

#include <vector>

#include "GObject.h"
#include "GMessage.h"

class GMessageBus : 
	public GObject
{
public:
	void postMessage(GMessage* msg);

private:
	// priority queue? priority linked list?
	std::vector<GMessage> _messages = {};
};

#endif GMESSAGEBUS_H