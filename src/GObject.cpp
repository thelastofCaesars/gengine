#include "GObject.h"

GObject::GObject()
{
	std::cout << "GObject: construted = " << this->name;
}

GObject::~GObject()
{
	std::cout << "GObject: deconstruted = " << this->name;
}

void GObject::setName(std::string name)
{
	this->name = name;
}

std::string GObject::getName()
{
	return this->name;
}