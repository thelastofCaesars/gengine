#pragma once
#ifndef GOBJECT_H
#define GOBJECT_H

#include <iostream>
#include <cassert>

class GObject
{
public:
	GObject();
	~GObject();

	void setName(std::string);
	std::string getName();

private:
	std::string name = "GObject";
};

#endif // GOBJECT_H