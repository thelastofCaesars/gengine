#include "GPoint3.h"

GPoint3::GPoint3()
{
	this->setName("Point3");
	this->zero();
	std::cout << "GPoint3: GPoint3 -  " << this->getName();
}

GPoint3::GPoint3(float x, float y, float z)
{
	this->setName("Point3");
	this->_x = x;
	this->_y = y;
	this->_z = z;
	std::cout << "GPoint3: GPoint3 -  " << this->getName();
}

GPoint3::GPoint3(float x, float y, float z, std::string name)
{
	this->setName(name);
	this->_x = x;
	this->_y = y;
	this->_z = z;
	std::cout << "GPoint3: GPoint3 -  " << this->getName();
}

GPoint3::~GPoint3()
{
	std::cout << "GPoint3: ~GPoint3 - " << this->getName();
}

void GPoint3::set(GPoint3* vector3)
{
	this->_x = vector3->_x;
	this->_y = vector3->_y;
	this->_z = vector3->_z;
}

void GPoint3::set(const GPoint3* vector3)
{
	this->_x = vector3->_x;
	this->_y = vector3->_y;
	this->_z = vector3->_z;
}

void GPoint3::set(float x, float y, float z)
{
	this->_x = x;
	this->_y = y;
	this->_z = z;
}

void GPoint3::setX(float x)
{
	this->_x = x;
}

void GPoint3::setY(float y)
{
	this->_y = y;
}

void GPoint3::setZ(float z)
{
	this->_z = z;
}

void GPoint3::zero()
{
	this->_x = 0.0f;
	this->_y = 0.0f;
	this->_z = 0.0f;
}

GPoint3* GPoint3::get()
{
	return this;
}

const GPoint3* GPoint3::get() const
{
	return this;
}

float GPoint3::getX()
{
	return _x;
}

float GPoint3::getY()
{
	return _y;
}

float GPoint3::getZ()
{
	return _z;
}