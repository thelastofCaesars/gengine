#pragma once
#ifndef GPOINT3_H
#define GPOINT3_H

#include <cassert>

#include "GObject.h"

class GPoint3 : public GObject
{

public:
	GPoint3();
	GPoint3(float x, float y, float z);
	GPoint3(float x, float y, float z, std::string name);
	~GPoint3();


	void set(GPoint3* point3);
	void set(const GPoint3* point3);
	void set(float x, float y, float z);
	void setX(float x); // consider using pointers!
	void setY(float y);
	void setZ(float z);
	void zero();

	GPoint3* get();
	const GPoint3* get() const;
	float getX();
	float getY();
	float getZ();

private:
	float _x, _y, _z;
};


#endif // GPOINT3_H