#include "Quaternion.h"
#include "GQuaternion.h"

Quaternion::Quaternion()
{
	this->_x = 0.0f;
	this->_y = 0.0f;
	this->_z = 0.0f;
	this->_w = 0.0f;
}

Quaternion::Quaternion(float x, float y, float z, float w)
{
	this->_x = x;
	this->_y = y;
	this->_z = z;
	this->_w = w;
}

Quaternion::Quaternion(const Quaternion* q)
{
	this->_x = q->_x;
	this->_y = q->_y;
	this->_z = q->_z;
	this->_w = q->_w;
}

void Quaternion::set(float x, float y, float z, float w)
{
	this->_x = x;
	this->_y = y;
	this->_z = z;
	this->_w = w;
}

Quaternion* Quaternion::get()
{
	return this;
}

const Quaternion* Quaternion::get() const
{
	return this;
}
