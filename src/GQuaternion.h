#pragma once
#ifndef GQUATERNION_H
#define GQUATERNION_H

#include <cassert>

#include "GObject.h"

class Quaternion
{
public:

	Quaternion();

	Quaternion(float x, float y, float z, float w);

	Quaternion(const Quaternion* q);

	void set(float x, float y, float z, float w);

	Quaternion* get();

	const Quaternion* get() const;

private:
	float _x;
	float _y;
	float _z;
	float _w;
};

#endif // GQUATERNION_H