#pragma once
#ifndef GSYSTEM_H
#define GSYSTEM_H

#include "GObject.h"
#include "GMessage.h"
#include "GMessageBus.h"

class GSystem :
	public GObject
{
public:
	void handleMessage(GMessage* msg);

private:
	GMessageBus* msgBus;
	//// Usage: msgBus->postMessage(msg);
};

#endif GSYSTEM_H