#include "GVector3.h"

GVector3::GVector3()
{
	this->setName("Vector3");
	this->zero();
	std::cout << "GVector3: GVector3 -  " << this->getName();
}

GVector3::GVector3(float x, float y, float z)
{
	this->setName("Vector3");
	this->_x = x;
	this->_y = y;
	this->_z = z;
	this->_magnitude = 0.0f;
	std::cout << "GVector3: GVector3 -  " << this->getName();
}

GVector3::GVector3(float x, float y, float z, std::string name)
{
	this->setName(name);
	this->_x = x;
	this->_y = y;
	this->_z = z;
	this->_magnitude = 0.0f;
	std::cout << "GVector3: GVector3 -  " << this->getName();
}

GVector3::~GVector3()
{
	std::cout << "GVector3: ~GVector3 - " << this->getName();
}

void GVector3::set(GVector3* vector3)
{
	this->_x = vector3->_x;
	this->_y = vector3->_y;
	this->_z = vector3->_z;
}

void GVector3::set(const GVector3* vector3)
{
	this->_x = vector3->_x;
	this->_y = vector3->_y;
	this->_z = vector3->_z;
}

void GVector3::set(float x, float y, float z)
{
	this->_x = x;
	this->_y = y;
	this->_z = z;
}

void GVector3::setX(float x)
{
	this->_x = x;
}

void GVector3::setY(float y)
{
	this->_y = y;
}

void GVector3::setZ(float z)
{
	this->_z = z;
}

void GVector3::zero()
{
	this->_x = 0.0f;
	this->_y = 0.0f;
	this->_z = 0.0f;
	this->_magnitude = 0.0f;
}

GVector3* GVector3::get()
{
	return this;
}

const GVector3* GVector3::get() const
{
	return this;
}

float GVector3::getX()
{
	return _x;
}

float GVector3::getY()
{
	return _y;
}

float GVector3::getZ()
{
	return _z;
}

float GVector3::Magnitude()
{
	if (_magnitude == 0.0f)
		_magnitude = GENmath::inv_sqrt(_x * _x + _y * _y + _z * _z);
	return _magnitude;
}

void GVector3::Normalize()
{
	float m = GENmath::inv_sqrt(this->Magnitude());
	this->set(this->_x * m, this->_y * m, this->_z * m);
}

void GVector3::Lerp(GVector3* vector3, float angle)
{
	this->set(
		(1.0f - angle) * this->getX() + angle * vector3->getX(), 
		(1.0f - angle) * this->getY() + angle * vector3->getY(), 
		(1.0f - angle) * this->getZ() + angle * vector3->getZ());
}

GVector3* GVector3::Lerp(GVector3* v1, GVector3* v2, float angle)
{
	GVector3* v = new GVector3(
		(1.0f - angle) * v1->getX() + angle * v2->getX(),
		(1.0f - angle) * v1->getY() + angle * v2->getY(),
		(1.0f - angle) * v1->getZ() + angle * v2->getZ());
	return v; // is it okay? wont it destroy until copied or sth?
}

GVector3 GVector3::operator+(GVector3 v)
{
	return GVector3(this->_x + v._x, this->_y + v._y, this->_z + v._z);
}

GVector3 GVector3::operator-(GVector3 v)
{
	return GVector3(this->_x - v._x, this->_y - v._y, this->_z - v._z);
}

GVector3 GVector3::operator*(GVector3 v) // check it (math)
{
	return GVector3(this->_x * v._x, this->_y * v._y, this->_z * v._z);
}

GVector3 GVector3::operator*(float f)
{
	return GVector3(this->_x * f, this->_y * f, this->_z * f);
}

GVector3 GVector3::operator/(GVector3 v) // check it (math)
{
	assert(v._x != 0.0f && v._y != 0.0f && v._z != 0.0f);
	return GVector3(this->_x / v._x, this->_y / v._y, this->_z / v._z);
}

GVector3 GVector3::operator/(float f)
{
	assert(f != 0.0f);
	return GVector3(this->_x / f, this->_y / f, this->_z / f);
}

void GVector3::operator+=(GVector3 v)
{
	this->set(this->_x + v._x, this->_y + v._y, this->_z + v._z);
}

void GVector3::operator-=(GVector3 v)
{
	this->set(this->_x - v._x, this->_y - v._y, this->_z - v._z);
}

void GVector3::operator*=(GVector3 v) // check it (math)
{
	this->set(this->_x * v._x, this->_y * v._y, this->_z * v._z);
}

void GVector3::operator*=(float f)
{
	this->set(this->_x * f, this->_y * f, this->_z * f);
}

void GVector3::operator/=(GVector3 v) // check it (math)
{
	assert(v._x != 0.0f && v._y != 0.0f && v._z != 0.0f);
	this->set(this->_x / v._x, this->_y / v._y, this->_z / v._z);
}

void GVector3::operator/=(float f)
{
	assert(f != 0.0f);
	this->set(this->_x / f, this->_y / f, this->_z / f);
}