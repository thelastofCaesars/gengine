#pragma once
#ifndef GVECTOR3_H
#define GVECTOR3_H

#include <iostream>
#include <cassert>

#include "GObject.h"
#include "GENmath.h"

class GVector3 : GObject
{
public:
	GVector3();
	GVector3(float x, float y, float z);
	GVector3(float x, float y, float z, std::string name);
	~GVector3();


	void set(GVector3* vector3);
	void set(const GVector3* vector3);
	void set(float x, float y, float z);
	void setX(float x); // consider using pointers!
	void setY(float y);
	void setZ(float z);
	void zero();

	GVector3* get();
	const GVector3* get() const;
	float getX();
	float getY();
	float getZ();

	float Magnitude();
	void Normalize();
	void Lerp(GVector3* vector3, float angle);
	GVector3* Lerp(GVector3* v1, GVector3* v2, float angle);

	GVector3 operator+(GVector3 v);
	GVector3 operator-(GVector3 v);
	GVector3 operator*(GVector3 v);
	GVector3 operator*(float f);
	GVector3 operator/(GVector3 v);
	GVector3 operator/(float f);
	void operator+=(GVector3 v);
	void operator-=(GVector3 v);
	void operator*=(GVector3 v);
	void operator*=(float f);
	void operator/=(GVector3 v);
	void operator/=(float f);
	

private:
	float _x, _y, _z;
	float _magnitude;
};
#endif // GVECTOR3_H